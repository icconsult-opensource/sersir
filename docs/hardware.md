# Hardware

Passend zu der Software wurde eine kleine Lochrasterplatine aufgebaut.  
Durch ein zusätzliches 12V-Netzteil zur Stromversorgung der von uns eingesetzten Lampe
wurde eine galvanische Trennung zwischen dem Stromkreis des Raspberry Pis (und dessen 5V USB-Netzteil)
und dem Stromkreis der Lampe notwendig. Dank der Hilfe von [Michael Renner (dd0ul)](https://twitter.com/dd0ul) 
wurde ein Schaltplan [Realität](https://twitter.com/dd0ul/status/786851125093888000) realisiert.  
  
[![Schaltplan von dd0ul](resources/schaltplan-dd0ul_small.jpg)](resources/schaltplan-dd0ul.jpg)

Die Übertragung auf eine Lochrasterplatine wurde im Hinblick auf mögliche Erweiterungen vorgenommen.  
Es können so sehr einfach mehrere Ausgänge parallel aufgebaut werden.  
[![Platinenlayout](resources/layout_small.png)](resources/layout.png)  

Das fertige Produkt sieht dann wie folgt aus:  
[![Platine von oben](resources/platine_oben_small.jpg)](resources/platine_oben.jpg)
[![Platine von unten](resources/platine_unten_small.jpg)](resources/platine_unten.jpg)


Folgende entscheidenden Komponenten wurden verbaut:

* 1 Optokoppler (4N35)
* PNP-Transistor (BD138)
* Widerstände: 47 Ohm, 4,7 kΩ, 100 kΩ, 1 MΩ  
