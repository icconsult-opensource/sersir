Der Pipeline Ansatz von Jenkins CI ist noch zu modern für das Auslesen der Coverage mit Cobertura,
so muss der klassische Ansatz mit einem Free Style Projekt und einem Shell-Skript verwendet werden.

Zum Aufsetzen eines CI-Jobs zum Beispiel folgendes Script benutzen:

```bash
# create virtual python environment
virtualenv -p $(which python3) env
export VIRTUALENV_PATH=$(readlink -f env) # get absolute path

# quirks to get current version of setuptools_scm, we need commit f51c6e302
${VIRTUALENV_PATH}/bin/pip install -U pip setuptools
git clone https://github.com/pypa/setuptools_scm.git setuptools_scm
cd setuptools_scm
${VIRTUALENV_PATH}/bin/python setup.py egg_info
${VIRTUALENV_PATH}/bin/pip install .
cd ../main

cd src
${VIRTUALENV_PATH}/bin/pip install -r requirements.txt -r requirements_dev.txt
# run code style checks
${VIRTUALENV_PATH}/bin/flake8
function pylint {
    # pylint exits code is composed as described https://pylint.readthedocs.io/en/latest/user_guide/run.html#exit-codes
    # we want to ignore 4, 8, 16
    set +e
    ${VIRTUALENV_PATH}/bin/pylint --output-format=text sersir/
    export RET_CODE=$?
    set -e
    if (( ${RET_CODE} & ~( 4 | 8 | 16 ) )); then
        return ${RET_CODE}
    fi
    echo ${RET_CODE}
    return 0
}
pylint

# run tests in the 'tests' sub folder
${VIRTUALENV_PATH}/bin/coverage run -m pytest --junitxml=pytest.xml tests
${VIRTUALENV_PATH}/bin/coverage report
# cobertura compatible output
${VIRTUALENV_PATH}/bin/coverage xml -o coverage.xml

# build and upload source package
${VIRTUALENV_PATH}/bin/python setup.py sdist
scp -B -p dist/* sersir@192.168.203.243:
```

Konfiguration eines ssh-keys für den Upload der source-Pakete und Beschränkung auf
die absolut notwendigste Berechtigungen. Wir verlassen uns auf das scp-Protokoll,
dass nur innerhalb dieses Ordners `/var/lib/sersir/packages` geschrieben werden kann:
```
no-agent-forwarding,no-port-forwarding,no-pty,no-user-rc,command="scp -t /var/lib/sersir/packages" ssh-rsa ...
```

Ab OpenSSH 7.2 kann auch nur folgendes verwendet werden. `restrict` deckt dabei auch zukünftige
Schlüsselwörter und Einstellungen ab.
```
restrict,command="scp -t /var/lib/sersir/packages" ssh-rsa ...
```

`deploy.sh` aus dem Verzeichnis `ansible/roles/sersir/templates` braucht um den entsprechenden
systemd Service ohne Passwort neustarten zu können eine entsprechende sudo-Regel:
```
sersir       ALL=(ALL:ALL) NOPASSWD:/bin/systemctl restart sersir.service
```
