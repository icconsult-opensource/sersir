import pytest
from sersir.utils import ping


def test_ping_fail():
    with pytest.raises(RuntimeError):
        ping('a.0.0.1', 3)


def test_ping_success():
    ping('127.0.0.1', 3)
