from sersir.base.runner import (
    detect_failed_jobs_changes,
    filter_job_names_fnpattern,
    filter_whitelist_jobs,
    filter_failed_jobs,
    filter_ignored_jobs
)


def test_detect_failed_jobs_changes():
    # 1. Fake of state.key to test detection
    state = {
        'Test_One': 0,
        'Test_Two': 1,
        'Test_Three': 2,
        'test_sdlkfjldk': 3234
    }
    job_failed_names = [
        'Name_One',
        'Name_Two',
        'Name_Three',
        'test_sdlkfjldk',
    ]

    # 2. Test Input/Output-Params (Comparison of params)
    state_new, new_failed, good_again = detect_failed_jobs_changes(state, job_failed_names)
    assert sorted(state_new.keys()) == sorted([
        'Name_One',
        'Name_Two',
        'Name_Three',
        'test_sdlkfjldk'
    ])
    assert new_failed == [
        'Name_One',
        'Name_Two',
        'Name_Three',
    ]
    assert sorted(good_again) == sorted([
        'Test_One',
        'Test_Two',
        'Test_Three',
    ])


def test_filter_job_names_fnpattern():
    job_names = ['proj', 'testproj', 'another-one']
    patterns = ['*pro*']

    assert {'proj', 'testproj'} == set(filter_job_names_fnpattern(job_names, patterns))


def test_filter_whitelist_jobs():
    jobs = [
        {
            'name': 'testproj1',
            'color': 'green'
        },
        {
            'name': 'project',
            'color': 'yellow',
        },
        {
            'name': 'another-project',
            'color': 'purple'
        },
        {
            'name': 'wildcard-project',
            'color': 'red'
        },
        {
            'name': 'wildcard-branch-xyz1',
            'color': 'orange'
        },
        {
            'name': 'wildcard-branch-xyz2',
            'color': 'blue'
        },
        {
            'name': 'another-wildcard-branch',
            'color': 'brown'
        }
    ]

    ignored_jobs = [
        'bla-blubb',
        'another-project',
        'wildcard-*',
    ]

    jobs_new = filter_whitelist_jobs(jobs, ignored_jobs)

    assert {job['name'] for job in jobs_new} == {'another-project', 'wildcard-project', 'wildcard-branch-xyz1', 'wildcard-branch-xyz2'}


def test_filter_ignored_jobs():
    # 1. Insertion of test-params
    jobs = [
        {
            'name': 'testproj1',
            'color': 'green'
        },
        {
            'name': 'project',
            'color': 'yellow',
        },
        {
            'name': 'another-project',
            'color': 'purple'
        },
        {
            'name': 'wildcard-project',
            'color': 'red'
        },
        {
            'name': 'wildcard-branch-xyz1',
            'color': 'orange'
        },
        {
            'name': 'wildcard-branch-xyz2',
            'color': 'blue'
        },
        {
            'name': 'another-wildcard-branch',
            'color': 'brown'
        }
    ]

    ignored_jobs = [
        'bla-blubb',
        'another-project',
        'wildcard-*',
    ]

    jobs_new = filter_ignored_jobs(jobs, ignored_jobs)

    assert {job['name'] for job in jobs_new} == {'testproj1', 'project', 'another-wildcard-branch'}


def test_filter_failed_jobs():
    # 1.  Insertion of test-params
    jobs = [
        {
            'name': 'testproj1',
            'color': 'green'
        },
        {
            'name': 'project',
            'color': 'yellow',
        },
        {
            'name': 'another-project',
            'color': 'purple'
        }
    ]
    failed_jobs = filter_failed_jobs(jobs)
    assert {job['name'] for job in failed_jobs} == {'project'}
