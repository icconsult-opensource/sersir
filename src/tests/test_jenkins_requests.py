import pytest

from sersir.jenkins import requests

config = {
    'host': 'my-host.local',
    'user': 'my-user',
    'token': 'token',
}


def mockreturn(url_parts):
    return (b'{"_class":"hudson.model.ListView","jobs":[{"_class":"hudson.model.FreeStyleProject","name":"test-sersir","color":"blue"},'
            b'{"_class":"org.jenkinsci.plugins.workflow.job.WorkflowJob","name":"test-sersir-pipeline","color":"blue"}]}')


def test_job_status(monkeypatch):
    monkeypatch.setattr(requests, 'read_url', mockreturn)

    assert requests.jobs_status(config['host'], config['user'], config['token']) == [{
        "_class": "hudson.model.FreeStyleProject",
        "name": "test-sersir",
        "color": "blue"
    }, {
        "_class": "org.jenkinsci.plugins.workflow.job.WorkflowJob",
        "name": "test-sersir-pipeline",
        "color": "blue"
    }]


def test_job_status_failures(monkeypatch):
    monkeypatch.setattr(requests, 'read_url', mockreturn)

    kwargs = {
        'host': 'test-local'
    }
    with pytest.raises(AttributeError):
        requests.jobs_status(**kwargs)

    kwargs['user'] = 'll'
    with pytest.raises(AttributeError):
        requests.jobs_status(**kwargs)

    del kwargs['host']
    with pytest.raises(AttributeError):
        requests.jobs_status(**kwargs)

    kwargs = {
        'host': 'test-local',
        'user': 'user',
        'token': 'my-token',
        'scheme': 'ssh'
    }
    with pytest.raises(AttributeError):
        requests.jobs_status(**kwargs)

    kwargs['scheme'] = 'http'
    with pytest.raises(RuntimeError):
        requests.jobs_status(**kwargs)

    kwargs['insecure'] = True
    with pytest.warns(RuntimeWarning):
        requests.jobs_status(**kwargs)
