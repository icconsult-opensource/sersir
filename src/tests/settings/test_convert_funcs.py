import pytest
from sersir.utils.setting_helper import (
    convert_string_to_timedelta,
    convert_string_to_debug_level,
    convert_string_to_list,
    check_list_of_strings,
    check_pi_pin_board_range,
    ConfigurationVariable
)
from datetime import timedelta
import logging


def test_check_pi_pin_board_range():
    conf_var = ConfigurationVariable('pin', int)
    conf_var.value = 1
    assert not check_pi_pin_board_range(conf_var)
    conf_var.value = 18
    assert check_pi_pin_board_range(conf_var)


@pytest.mark.parametrize('time_string, expected', [
    ('5s', timedelta(seconds=5)),
    ('0m6s', timedelta(seconds=6)),
    ('0h0m7s', timedelta(seconds=7)),
    ('0hr0m7s', timedelta(seconds=7)),

    ('0h2m0s', timedelta(minutes=2)),
    ('0h1m', timedelta(minutes=1)),
    ('0hr1m', timedelta(minutes=1)),
    ('3m', timedelta(minutes=3)),

    ('1h', timedelta(hours=1)),
    ('1hr', timedelta(hours=1)),
    ('2h0m', timedelta(hours=2)),
    ('2hr0m', timedelta(hours=2)),
    ('3h0m0s', timedelta(hours=3)),
    ('3hr0m0s', timedelta(hours=3)),

    ('1m2s', timedelta(minutes=1, seconds=2)),
    ('0h3m4s', timedelta(minutes=3, seconds=4)),
    ('0hr3m4s', timedelta(minutes=3, seconds=4)),

    ('1h2s', timedelta(hours=1, seconds=2)),
    ('3h0m4s', timedelta(hours=3, seconds=4)),
    ('3hr0m4s', timedelta(hours=3, seconds=4)),

    ('1h2m', timedelta(hours=1, minutes=2)),
    ('3h4m0s', timedelta(hours=3, minutes=4)),
    ('3hr4m0s', timedelta(hours=3, minutes=4)),

    ('1h2m3s', timedelta(hours=1, minutes=2, seconds=3)),
    ('1hr2m3s', timedelta(hours=1, minutes=2, seconds=3)),
    pytest.mark.xfail(('31424', None), raises=ValueError, strict=True),
])
def test_convert_string_to_timedelta(time_string, expected):
    assert convert_string_to_timedelta(time_string) == expected


@pytest.mark.parametrize('level_string, expected', [
    ('cRitiCAl', logging.CRITICAL),
    ('eRRor', logging.ERROR),
    ('WarNIng', logging.WARNING),
    ('inFO', logging.INFO),
    ('dEBuG', logging.DEBUG),
    ('nOtSEt', logging.NOTSET),

    ('critical', logging.CRITICAL),
    ('error', logging.ERROR),
    ('warning', logging.WARNING),
    ('info', logging.INFO),
    ('debug', logging.DEBUG),
    ('notset', logging.NOTSET),
    pytest.mark.xfail(('sechshundertsechsundsechszig', None), raises=ValueError, strict=True)
])
def test_convert_string_to_debug_level(level_string, expected):
    assert convert_string_to_debug_level(level_string) == expected


@pytest.mark.parametrize('test_value, expected', [
    (
        'my-project,myThirdProject,mySecondOne',
        ['my-project', 'myThirdProject', 'mySecondOne']
    ),
    (
        'my-project,myThirdProject,mySecondOne,myFourth',
        ['my-project', 'myThirdProject', 'mySecondOne', 'myFourth']),
    (
        '        my-project    ,\nmyThirdProject  ,\tmySecondOne',
        ['my-project', 'myThirdProject', 'mySecondOne']
    ),
    (
        'my-project\n\n\n,\t\tmyThirdProject,mySecondOne,myFourth',
        ['my-project', 'myThirdProject', 'mySecondOne', 'myFourth']
    ),
    (15, 15),
])
def test_convert_string_to_list(test_value, expected):
    assert convert_string_to_list(test_value) == expected


@pytest.mark.parametrize('test_value, success', [
    (['str1', 'str2', 'str3', 'str4'], True),
    (('str!', 'str2', 'str3'), False),
    (['str1', 56, r'/.*/'], False)
])
def test_check_list_of_strings(test_value, success):
    var = ConfigurationVariable('test', list)
    var.value = test_value
    assert check_list_of_strings(var) == success


class TestConfigurationVariable:
    @pytest.mark.parametrize('test_type, test_value', [
        (int, 50),
        (str, "myString"),
        (list, []),
        (tuple, ('str', 'upps')),
        (dict, {'myDict': 'is my one'}),
        (float, 3.3),
    ])
    def test_type_validation(self, test_type, test_value):
        var = ConfigurationVariable('test', test_type)
        var.value = test_value
        var.validate()
        assert var.validated

    def test_convert_func(self):
        def int_to_float(val):
            return float(val)

        var = ConfigurationVariable('test', float, convert_func=int_to_float)
        var.value = 4
        var.validate()
        assert var.validated
        assert var.value == 4.

    def test_validation_func(self):
        def is_number_type(conf_var: ConfigurationVariable):
            val = conf_var.value
            return type(val) is int or type(val) is float

        var = ConfigurationVariable('test1', float, validation_func=is_number_type)
        var.value = 5
        var.validate()
        assert var.validated
        var.value = 5.
        var.validate()
        assert var.validated
