#!/bin/bash
# {{ ansible_managed }}

set -e

PACKAGE_PATH="{{ packages_dir }}"
INSTALLED_PACKAGES="${PACKAGE_PATH}/installed"
INSTALLED_PACKAGES_USE_SUBDIRS=1

VIRTUALENV_PATH="{{ virtualenv_dir }}"

PKG_NAMES="sersir"

INOTIFY_DELAY=10

###############

ONE_RUN=1
DAEMON=0
DISPLAY_HELP=0

while [[ $# -gt 0 ]]; do
	key="$1"

	case "${key:?}" in
		-d|--daemon)
			DAEMON=1
		;;
		-h|--help)
			DISPLAY_HELP=1
			ONE_RUN=0
		;;
		--no-initial-run)
			ONE_RUN=0
		;;
		--inotify-delay)
		    INOTIFY_DELAY=$2
		    shift
		;;
		*)
			echo "Argh, unknown option \"${key}\""
			DISPLAY_HELP=1
			ONE_RUN=0
			break
		;;
	esac
	shift # past argument or value
done


function display_help {
	echo "I'm installing some pip packages with a local repository"
	echo "I have the following flags:"
	echo "    -h|--help             display this help text"
	echo "    -d|--daemon           watch the packages directory for changes with inotify and run forever"
	echo "    --no-initial-run      don't do an initial run"
}

function block_for_change {
    # Make use of the inotify interface of the linux kernel
    # The call to inotifywait blocks until actions of the given types occur
	inotifywait -e modify,move,create "${PACKAGE_PATH:?}"
}

function deploy {
    # Define the following lists local to avoid issues running the function multiple times
	local PIP PACKAGE_LIST NUM_PACKAGES TMP_DIR PACKAGE SUBDIR MV_PATH

    # Shortcut for the pip executable
	PIP="${VIRTUALENV_PATH:?}/bin/pip"

    # get files in PACKAGE_PATH and count them because we do not want to implement a lock to the directory...
	PACKAGE_LIST="$(find ${PACKAGE_PATH:?} -maxdepth 1 -type f)"
	NUM_PACKAGES="$(find ${PACKAGE_PATH:?} -maxdepth 1 -type f | wc -l)"

	if (( ${NUM_PACKAGES} > 0 )); then
		echo "There are ${NUM_PACKAGES} laying around since my last run"

        # ... so get a temporary directory for the packages
		TMP_DIR="$(mktemp -d)"

        # transfer packages to the temporary dir
		for PACKAGE in ${PACKAGE_LIST}; do
			PACKAGE="$(basename ${PACKAGE})"
			mv "${PACKAGE_PATH}/${PACKAGE}" "${TMP_DIR}"
		done

        # let pip extract the version number of the packages in the tmp dir
        # and try to upgrade the most recent version of the packages specified by PKG_NAMES
		${PIP} install --upgrade --find-links "${TMP_DIR}" ${PKG_NAMES}

        # create a subpath for the packages used here if requested
		if [[ "${INSTALLED_PACKAGES_USE_SUBDIRS}" == "1" ]]; then
			SUBDIR="d$(date -u +%Y-%m-%d_%H-%M-%S)"
			MV_PATH="${INSTALLED_PACKAGES}/${SUBDIR}"
			mkdir -p ${MV_PATH}
		else
			MV_PATH="${INSTALLED_PACKAGES}"
		fi

        # move back package files and tidy up the tmp dir
		for PACKAGE in ${PACKAGE_LIST}; do
			PACKAGE="$(basename ${PACKAGE})"
			mv "${TMP_DIR}/${PACKAGE}" "${MV_PATH}"
		done

		rmdir "${TMP_DIR}"

        # restart our fancy service <3
		sudo systemctl restart "{{ main_systemd_unit }}"
	else
		echo "Nothing to do"
	fi
}

if [[ "${DISPLAY_HELP}" == "1" ]]; then
	display_help
fi

if [[ "${ONE_RUN}" == "1" ]]; then
	deploy
fi

if [[ "${DAEMON}" == "1" ]]; then
	while block_for_change; do
		sleep ${INOTIFY_DELAY:-10} # Wait to complete a second file transfer or so
		deploy
	done
fi
