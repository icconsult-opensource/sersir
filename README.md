# SerSir

## Was ist das?

Die Zielsetzung dieses kleinen Projektes war die Verbesserung der Wahrnehmung von fehlgeschlagenen Jobs
eines Jenkins-CI-Servers. Oft werden Projekte erst nach größerer Zeit ausgebessert.

Generell kann dieses Projekt dazuverwendet werden einen Programm-Trigger für das Leuchten einer Lampe zu verwenden.
Inhalt ist, dass ein Python-Programm, welches die Jenknis-Job-API auswertet und eine GPIO-Pin ansteuert. Von dem Pin
wird die erstellte Schaltung und damit die Lampe angesteuert.

## Anforderungen

* Raspberry Pi, getestet mit v3 Modell B
* raspbian, getestet mit jessie
* python3, getestet mit v3.4
* pip, getestet mit v9

## deployment

### ansible role

Es liegt eine ansible-Rolle bereit: ```ansible/roles/sersir```  
Beispiel für ein Playbook:

    ---
    - hosts: sersir
      become: true
      become_user: root
      roles:
        - sersir

### step-by-step

- Installieren der benötigten Pakete im System  
    ```root@sersir-dev:~# apt-get install python3 python3-dev```
- Neuen User anlegen - muss die Berechtigung haben, die GPIO-Pins anzusteuern
  (in Raspbian Mitglied der Gruppe GPIO sein)  
    ```useradd -G gpio -m -s $(which bash) sersir```
- Den Kontext des neuen Benutzers annehmen  
    ```su - sersir``` o.ä.
- Virtuelle python-Umgebung anlegen und betreten  
    ```python3 -m venv env && source env/bin/activate```
- Updates von ```pip``` und ```setuptools``` überprüfen  
    ```pip install --upgrade pip setuptools```
- ```SerSir``` installieren mit ```systemd```  
    ```pip install sersir[systemd]```

## development

### Codestyle

Zur Einhaltung des python-Codestyles werden mit flake8, welches pycodestyle (ehemals pep8) einbindet,
die Regeln der python-Community überprüft. pylint wird detaillierter und gibt zusätzlich noch Empfehlungen,
falls nicht python idiomatische Anweisungen gebraucht werden. Hinweise zu Warnungen, Refactor und Konventionen
werden in der Empfehlung für die CI-Integration ignoriert. Alles andere Meldungen müssen behoben werden.

Daher sollten beim Entwickeln, falls der genutzte Editor und die genutzte Entwicklungsumgebung dies nicht
direkt unterstützt, die entsprechenden Tools regelmäßig ausgeführt werden. Die Aufrufe können aus der 
CI-Dokumentation entnommen werden.  
Die Tools müssen vor einem Commit auf entsprechende Fehlermeldungen überprüft werden.

### development setup

```
pi@sersir-dev:~$ mkdir demo
pi@sersir-dev:~$ cd demo/
pi@sersir-dev:~/demo$ git clone https://bitbucket.org/icconsult/sersir.git
    Cloning into 'sersir'...
    remote: Counting objects: 535, done.
    remote: Compressing objects: 100% (243/243), done.
    remote: Total 535 (delta 249), reused 505 (delta 236)
    Receiving objects: 100% (535/535), 76.00 KiB | 0 bytes/s, done.
    Resolving deltas: 100% (249/249), done.
    Checking connectivity... done.
pi@sersir-dev:~/demo$ python3 -m venv env
pi@sersir-dev:~/demo$ source env/bin/activate
    (env) pi@sersir-dev:~/demo$ pip install -U pip setuptools
    Downloading/unpacking pip from ...
      Downloading pip-9.0.1...
    Downloading/unpacking setuptools from ...
      Downloading setuptools-34.3.1...
    ...
    Successfully installed pip setuptools packaging appdirs six pyparsing
    Cleaning up...
(env) pi@sersir-dev:~/demo$ cd sersir/src/
(env) pi@sersir-dev:~/demo/sersir/src$ pip install -r requirements.txt -r requirements_dev.txt
    ....
    Successfully installed RPi.GPIO coverage flake8 pylint pytest systemd ...
(env) pi@sersir-dev:~/demo/sersir/src$ flake8 && echo "Success"
    Success
(env) pi@sersir-dev:~/demo/sersir/src$ pylint sersir/
    ..............
    Global evaluation
    -----------------
    Your code has been rated at 9.39/10 (previous run: 9.39/10, +0.00)
(env) pi@sersir-dev:~/demo/sersir/src$ echo $?
    28      # 16 + 8 + 4 (convention, refactor & warning message issued and could be ignored)
```

## Konfiguration

Das Projekt benötigt folgende Variablen, welche über die dann nachfolgend beschriebenen Mechanismen
gesetzt werden können:

* ```scheme```: ```https``` oder auch ggf. ```http``` für den Zugriff auf Jenkins
* ```host```: Der Hostname des Jenkins, z.B. ```jenkins-host.local```
* ```path```: Falls der Jenkins in einem Subpfad betrieben wird, muss dieser hier angegeben werden,
   z.B. ```/``` für den Standardwert
* ```user```: Der Username des zu nutzenden Nutzers
* ```token```: Das dem User zugehörige API-Token

* ```sleep_time```: Die Zeitspanne zwischen zwei Abfragen, in der das Programm schläft;
    als ```datetime.timedelta``` oder im Format ```0h0m0s```
* ```audit_log_file```: Der absolute oder zum Arbeitsverzeichnis relativer Pfad für die Audit Log Datei
* ```state_file```: Der absolute oder zum Arbeitsverzeichnis relativer Pfad für die ```state.json```

* ```debug_level```: Ein Wert aus dem [```logging```-Modul](https://docs.python.org/3/library/logging.html#logging-levels) von Python:
    * ```CRITICAL```
    * ```ERROR```
    * ```WARNING```
    * ```INFO```
    * ```DEBUG```
    * ```NOTSET```

* ```lamp_gpio_board_pin```: Der Pin nach dem Board-Layout des Raspberry, welcher bei fehlgeschlagenen
   Builds auf High geschaltet werden soll
* ```ignored_projects```: Liste von Strings mit Namen von ignorierten Projekten
* ```whitelisted_projects```: Liste von Strings mit Namen von gewünschten Projekten

Folgende Möglichkeiten um diese Konfigurationsvariablen zu setzen bestehen:
1. Über Umgebungsvariablen:  
   Der Name der Variable wird in Großbuchstaben konvertiert und ```SERSIR_``` vorangestellt.
2. Über ein python-Modul, welches über den normalen search-path unter ``` SERSIR_SETTINGS_MODULE``` versucht wird zu laden.
3. Über eine python-Datei, welche am festen Platz ```sersir.config``` zu finden ist.

Falls eine Variable über mehrere Möglichkeiten gesetzt worden sein sollte, gilt die obige Reihenfolge auch für die Priorität,
d.h., dass die Umgebungsvariablen am höchsten priorisiert sind und die alte python-Datei am statischen Platz die niedrigste
Priorität bekommt.

### Beispiel

```python
scheme = 'https'
host = 'jenkins-host.local'
path = '/'

user = 'jenkins-server'
token = ''

import datetime
sleep_time = datetime.timedelta(seconds=30)

audit_log_file = 'audit.log'
state_file = 'state.json'

import logging
debug_level = logging.INFO

lamp_gpio_board_pin = 1  # number of the gpio pin according the board scheme

ignored_projects = ['project_name']
```
